# Activitat 4. Arquitectures de protocols: el model OSI



## Qüestió 1

Per què no s'utilitza el model OSI?

1. La complexitat del model, innecessària en molts casos.
2. La complexitat de les normes desenvolupades a partir del model.
3. L'impuls del model Internet i la simplicitat dels seus estàndards.
4. Totes les respostes anteriors són correctes.

## Qüestió 2

Quin es l'ordre correcte dels nivells del model OSI?

1. Físic, Enllaç, Xarxa, Transport, Sessió, Presentació i Aplicació.
2. Enllaç, Transport, Xarxa, Físic, Sessió, Presentació i Aplicació.
3. Físic, Xarxa, Enllaç, Sessió, Transport, Presentació i Aplicació.
4. Físic, Xarxa, Transport, Enllaç, Sessió, Presentació i Aplicació.

## Qüestió 3

En quantes direccions es comuniquen els nivells?

1. 3
2. 4
3. 2
4. Cap.

## Qüestió 4

Quan es dóna la comunicació horitzontal?

1. Només es dóna entre nivells no adjacents.
2. Només es dóna entre nivells adjacents d'un mateix sistema.
3. No es dóna mai.
4. Només es dóna entre nivells homònims.

## Qüestió 5

Quan parlem del servei de nivell 3 volem dir que:

1. L'entitat del nivell 2 proporciona el servei i l'entitat del nivell 3 l'utilitza.
2. L'entitat del nivell 3 proporciona el servei i l'entitat del nivell 4 l'utilitza.
3. L'entitat de nivell 4 proporciona el servei i l'entitat del nivell 3 l'utilitza.
4. Cap de les respostes anteriors és correcta.

## Qüestió 6

Què entens per servei?

1. Per servei entenem la comunicació que es produeix dins d’una mateixa màquina i, per tant, dins d’un únic àmbit de responsabilitat.
2. Per servei entenem un sistema electrònic i/o informàtic, ubicat dins d’un nivell del model OSI, que, en combinació amb les altres entitats del mateix nivell situades en altres sistemes, formen un tot.
3. Per servei entenem la recepció d’una seqüència de bits en un moment inesperat o d’una longitud incorrecta, o en una disposició imprevista.
4. Per servei entenem un sistema distribuït.

## Qüestió 7

Què pretenen els protocols?

1. Intercomunicació de servidors.
2. Intercomunicació de serveis.
3. Intercomunicació per P2P.
4. Intercomunicació d'entitats situades en màquines diferents.

## Qüestió 8

En terminologia OSI es sol dir que:

1. Els serveis no es descriuen, sinó que s'especifiquen
2. Els serveis ni es descriuen ni s' especifiquen
3. Els serveis no s'especifiquen sinó que es descriuen
4. Els serveis s'especifiquen i es descriuen

## Qüestió 9

Quina relació hi ha entre els protocols i els nivells?

1. Hi ha més nivells que protocols.
2. Hi ha el mateix número de nivells que de protocols.
3. Hi ha més protocols que nivells.
4. No tenen cap relació.

## Qüestió 10

Quan es detecta un error al nivell físic que fa internament la capa?

1. Repara l'error i envia el paquet al següent nivell sense problemes.
2. És incapaç de reconèixer l'error i es perd el paquet.
3. Assumeix una probabilitat d'error i encarrega al nivell superior la correcció.
4. Reconeix l'error, destrueix el paquet i avisa l'usuari que revisi el paquet i el torni a
enviar.

## Qüestió 11

Els medis de transmissió tenen una capacitat de transmissió limitada i
l'electrònica que utilitzem per dur a terme les transmissions pot millorar la velocitat de
transmissió, però no superar aquest límit. Com ve determinada aquesta limitació?

1. Per l'ample de banda.
2. Per l'amplada de l'espectre elèctric.
3. Per la impossibilitat pràctica de rebre el senyal lliure de qualsevol interferència.
4. Totes les respostes anteriors són correctes.

## Qüestió 12

El nivell físic s'encarrega de...

1. Les tasques de transmissió física dels senyals elèctrics (o
electromagnètics) entre els diferents sistemes.
2. El programari, algorismes i protocols.
3. Les tasques de transmissió física dels senyals de transmissió i recepció.
4. Les tasques de transmissió física dels senyals elèctrics (o electromagnètics) en un
mateix sistema.

## Qüestió 13

Les limitacions del nivell físic n’imposen d’altres a la resta del sistema:

1. D’una banda, limiten la velocitat de transmissió i, de l’altra, fan aparèixer una probabilitat d’error.
2. D’una banda, limiten la velocitat de transmissió i, de l’altra, imposen un límit
superior.
3. D’una banda, fan aparèixer una probabilitat d’error i, de l’altra, limiten la possibilitat pràctica de rebre el senyal amb interferències.
4. Limiten al 30% les funcions del nivell d'enllaç.


## Qüestió 14

En quin tipus d'estructura s'agrupen els bits a la capa d'enllaç?

1. En bytes.
2. En datagrames.
3. En trames.
4. En paquets de 32 bits.

## Qüestió 15

Quines tasques duu a terme el nivell d'enllaç?

1. Permet una connexió fiable sobre qualsevol tipus de xarxa.
2. Control d'errors i de flux.
3. Filtrar i organitzar els bits.
4. Organitzar les trames.

## Qüestió 16

Què és la detecció d'errors?

1. Control dels paquets de 32 bits de l'estructura de la capa.
2. Delimitació i estructuració dels paquets.
3. Els caràcters a comprovar s'agrupen en files i columnes en aquest procés.
4. Addició de bits redundants i la seva comparació en la recepció.

## Qüestió 17

El control de flux fa que:

1. El transmissor envii trames molt curtes per tal de detectar millor els errors.
2. El receptor envii al transmissor un senyal que indica que ha rebut les trames correctament
3. El receptor notifiqui al transmissor que pari la transmissió momentàniament.
4. La transmissió es produeixi en un temps determinat.

## Qüestió 18

Si només tenim el nivell d'enllaç, podem connectar dues màquines que estan en
diferents xarxes?

1. Sí, si es connecten amb cable creuat.
2. Sí, perquè només estem connectant dues màquines.
3. Es poden connectar totes les màquines que es desitgi sempre i quan no s'esgotin les IP's.
4. No, només es poden connectar les màquines que estan a la mateixa xarxa.

## Qüestió 19

Una xarxa que funcioni en mode circuit virtual permet:

1. Garantir el lliurament de paquets al destinatari.
2. Agrupar els paquets relacionats.
3. Evitar problemes d'ordre, duplicació, o pèrdua.
4. Totes les anteriors son correctes.

## Qüestió 20

Quin o quins son els tipus de xarxes de commutació de paquets?

1. Xarxes que funcionen en mode datagrama i Xarxes que funcionen en mode circuit virtual.
2. Xarxes que funcionen en mode datagrama.
3. Xarxes que funcionen en mode circuit virtual.
4. Cap de les anteriors es correcta.

## Qüestió 21

De què s'encarrega el nivell de xarxa?

1. Transportar paquets des de l'origen fins al destí.
2. Només connecta màquines situades a la mateixa xarxa.
3. Garanteix que els bits enviats són rebuts pel destinatari.
4. S’encarrega de les tasques de transmissió física dels senyals elèctrics.

## Qüestió 22

L'assignació de direccions i direccionament són tasques de la capa:

1. 1
2. 2
3. 3
4. 4

## Qüestió 23

Quin tipus de xarxa de commutació és més fiable?

1. Les xarxes que funcionen en mode datagrama.
2. Les xarxes que funcionen en mode circuït virtual.
3. Les dues són igual de fiables.
4. Cap de les dues és fiable.

## Qüestió 24

El direccionament ens permet:

1. Conduir la informació des de l'origen fins al destí, encriptant les dades.
2. Conduir la informació des de l'origen fins al destí, minimitzant el trajecte.
3. Conduir la informació des de l'origen fins al destí, controlant l'itinerari recorregut.
4. Conduir la informació des de l'origen fins al router més proper al destí.

## Qüestió 25

En una xarxa que funcioni en forma datagrama, qui garanteix l'entrega correcta i
completa de la informació?

1. La mateixa xarxa.
2. El nivell de xarxa.
3. El nivell d'enllaç.
4. Els nivells superiors.

## Qüestió 26

Què permet el nivell de transport?

1. Que hi pugui haver més de dues màquines involucrades en les interconnexions.
2. Una connexió fiable sobre qualsevol tipus de xarxa.
3. Gestionar l'encriptació dels paquets transportats per la xarxa.
4. Controlar la seguretat en la recepció de dades.

## Qüestió 27

Quina es la funció principal del nivell de transport?

1. Mostrar la qualitat de transmissió entre els terminals que utilitzen la xarxa.
2. Aconseguir que les diferents plataformes es puguin entendre en connectar-se per mitjà d’una mateixa xarxa.
3. Assegurar la qualitat de transmissió entre els terminals que utilitzen la xarxa.
4. Permetre que els paquets viatgin per la xarxa des d’una estació terminal a una altra.

## Qüestió 28

Quina funció no realitza el nivell de sessió?

1. Gestionar les connexions de llarga durada.
2. Recuperar-se de caigudes de xarxa de manera transparent.
3. Recuperació de la IP donada pel servei DHCP.
4. Gestionar els protocols de sincronia entre aplicacions.

## Qüestió 29

De què s'encarrega el nivell de presentació?

1. De que les plataformes s'entenguin per mitjà d'una xarxa diferent.
2. De que les diferents plataformes s'entenguin mitjançant una mateixa xarxa.
3. De comprimir les diferents informacions i descodificar-les.
4. Per trobar servidors a una xarxa diferent.

## Qüestió 30

Quina capa precedeix les altres?

1. Aplicació
2. Sessió
3. Presentació
4. Cap de les tres són capes


## Qüestió 31

El nivell d'aplicació:

1. Assegura la qualitat de transmissió entre els terminals que utilitzen la xarxa.
2. Assigna adreces, decideix quin dels múltiples terminals és el destinatari final de cada paquet.
3. Hi resideixen els programes. Trobem servidors, clients que hi accedeixen, aplicacions que treballen segons un model simètric,...
4. Permet que hi pugui haver més de dues màquines involucrades en les interconnexions.

## Qüestió 32

A quin nivell es troben els servidors?

1. Per sota el nivell de transport
2. Nivell de presentació
3. Nivell d'aplicació
4. Entre els nivells de presentació i aplicació

## Qüestió 33

Quantes capes té el model TCP/IP?

1. 3
2. 4
3. 5
4. El model TCP/IP no està estructurat amb capes

## Qüestió 34

Quina relació hi ha entre la capa d'aplicació del model OSI i la capa d'aplicació del model TCP/IP?

1. És la mateixa capa.
2. La capa d'aplicació del model OSI engloba les capes de sessió, presentació i aplicació del model TCP/IP.
3. La capa d'aplicació del model TCP/IP engloba les capes de sessió, presentació i aplicació del model OSI.
4. No tenen cap relació.
