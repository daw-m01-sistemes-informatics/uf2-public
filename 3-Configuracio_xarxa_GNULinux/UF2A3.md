# Connexió a una xarxa local i a Internet



## Exercici 1

Descriu breument cadascun dels següents conceptes:

1. Interfície de xarxa
2. Adreça MAC
3. Adreça IP
4. Màscara de subxarxa
5. Adreça Broadcast
6. Default gateway (porta d'enllaç predeterminada)
7. Servei DHCP
8. Servei DNS

## Exercici 2

Digues quines comandes has de fer servir per esbrinar les següents dades del
teu ordinador (i enumera quines són aquestes dades en el teu cas particular):

1. Adreça MAC
2. Adreça IP
3. Màscara de subxarxa
4. Adreça Broadcast
5. Default gateway (porta d'enllaç predeterminada)
6. Servidor DHCP
7. Servidor DNS

## Exercici 3

Quina és el significat de l'adreça `255.255.255.255`? Seria l'adreça broadcast de
la xarxa zero `0.0.0.0`? I si és així, quina és la xarxa `0.0.0.0`?

## Exercici 4

Configureu una petita xarxa local (temporal), una per fila**¹** (3 ordinadors)
seguint els següents passos (enumereu la/les comanda/es que feu servir per a
cada pas):

1. Afegiu una IP a cadascuna de les màquines.

2. Comproveu la connexió entre els ordinadors de la mateixa xarxa.

3. Afegiu les rutes necessàries als ordinadors per a que tots els pc's de la
   taula A puguin fer ping (amb resposta) a qualsevol ordinador de la taula B.

4. Resoleu el mateix exercici d'abans però ara fent servir un default gateway.


## Exercici 5

Enumera quins serien els passos a seguir si volguéssim que la xarxa que heu
configurat a l'exercici 3 fos una configuració permanent sempre en els següents
dos casos:

1. La configuració està fixada i el servei de xarxa està actiu en iniciar
   l'ordinador.

2. La configuració està fixada però el servei de xarxa no està actiu en iniciar
   l'ordinador.

## Exercici 6

Quin és el fitxer de configuració de la interfície del teu ordinador?
Interpreta cadascuna de les seves directives.

## Exercici 7

Respecte el servei DNS:

1. Què faries per afegir un servidor DNS a la teva configuració?

2. Què vol dir la directiva search del fitxer de configuració de DNS?

3. Tinc connexió de xarxa si no tinc cap servidor DNS configurat? Quines
   mancances tinc?

4. Si comenteu les línies del fitxer /etc/resolv.conf , es podria accedir amb
   el navegador a l'adreça https://www.debian.org ? Que hauries de fer abans de
   comentar les línies d'aquest fitxer ?


## Exercici 8

Imagina que tens la següent xarxa:

![diagrama xarxa exercici8](exercici8.png)

Explica quins passos seguiries per:

1. Demanar una IP al router per al PC-B sense que quedi com a configuració
   permanent.

2. Demanar una IP al router per al PC-B sempre que s'engegui l'ordinador.
 
3. Connectar temporalment el PC-A, el PC-D i el PC-E a la xarxa assignant-los
   les IP's senyalades, fent que tingui sortida a Internet i suposant que el
   ROUTER fa de servidor DNS.

4. Fer que el PC-A, el PC-D i el PC-E tinguin sempre les IP's senyalades,
   sortida a Internet i, suposant que el ROUTER fa de servidor DNS.

5. Fes un script que puguem executar des del PC-F per tenir sortida a Internet
   sabent que tenim servei DHCP i que la connexió wireless és oberta amb
   l'essid open.

## Nota

¹ Per exemple:

+ la fila que té els pc's 1-2-3 serà la fila 1
+ la fila que té els pc's 4-5-6 serà la fila 2
+ llavors fem que l'ordinador amb número 2, tindrà IP 10.0.1.2 (l'1 és la fila 1 i el 2 del pc 2)
+ o el pc 6 tindrà com a IP 10.0.2.6 (el 2 és la fila 2 i el 6 del pc 6).
+ manca la màscara per que cada fila (diferent) sigui una xarxa (diferent).

