# Activació/desactivació peticions icmp broadcast


Quan un host rep una petició de ping feta com a broadcast, l'ordinador
respondrà o no en funció del valor que tingui emmagatzemmat al fitxer
`/proc/sys/net/ipv4/icmp_echo_ignore_broadcasts`

+ Si el valor és 1 NO respondrà 
+ Si el valor és 0 respodrà.

La 1a opció és la que es considera més segura. Si pel contrari tenim activada
la resposta a aquest tipus de pings a la nostra xarxa es podria produir un
tipus d'atac DoS (denial of service) que es diu [_Smurf
attack_](https://en.wikipedia.org/wiki/Smurf_attack)

Si volem correr aquest risc, podem fer de manera temporal:

```
echo "0" >  /proc/sys/net/ipv4/icmp_echo_ignore_broadcasts
```

o equivalentment:

```
sysctl -w net.ipv4.icmp_echo_ignore_all=0
```

Les ordres anteriors són temporals, per tant quan tornem a engegar la màquina
el valor tornarà al default.

Si volguessim que el canvi fos definitiu (en principi no recomanable) hauríem d'afegir al fitxer /etc/sysctl.conf`:

```
net.ipv4.icmp_echo_ignore_all = 1
```

i perquè el canvi tingui efecte immediat i no calgui reiniciar la màquina:

```
sysctl -p
```

