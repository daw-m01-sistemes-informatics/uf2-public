# Activitat 0. Terminologia

## Exercici 1. 

+ Què és la
  *[netiquette](https://ca.wikipedia.org/wiki/Etiqueta_(societat_de_la_informaci%C3%B3))*?

+ Posa un exemple de
  *[top-posting](https://ca.wikipedia.org/wiki/Top-posting)*.

+ Què és *[crossposting](https://ca.wikipedia.org/wiki/Enviament_creuat)*?  º
  Llegiu els següents dos articles que parlen de com comportar-se a Internet: 

+ Posa un parell regles que et semblin interessants de [les normes d'etiqueta
  en el correu
electrònic](https://pangea.org/area-dusuari/normes-detiqueta-en-el-correu-electronic/)?

+ [Contamina fer aquest exercici](https://www.apuntmedia.es/noticies/societat/contamina-llegir-noticia-cost-mediambiental-d-internet-alt-et-penses_1_1406047.html)? Desprès de llegir la notícia respon al fòrum seguint les indicacions de l'exercici següent.


## Exercici 2.

Imagineu-vos la següent situació: 

> Acabes de començar la UF2 de Fonaments de Xarxes. En la primera classe de la UF de xarxes a un vídeo surt un router i un switch. No tens clar que fa un router, ni un switch i menys encara la diferència entre els dos, però et fa vergonya preguntar-ho perquè sembla que tothom ho hagi entès... Com que et quedes amb el neguit decideixes enviar un correu a una llista especialitzada per resoldre els teus dubtes. 

1. Envia un correu/missatge al fòrum de la Netiquette amb el dubte exposat. 

2. Envia un altre correu/missatge al fòrum de la Netiquette donant la teva opinió
   respecte a si fer aquest exercici contamina o no.

**Important: Un correu ha de complir la Netiquette i l'altre no.**

